import pandas as pd
import numpy as np


def import_videos(path, db_conn, helpers):
    try:
        df = pd.read_csv(path, delimiter=";", dtype=str)
    except:
        msg = "ERROR: could not read from file: \"" + path + "\""
        exit(msg)

    total_rows = df.shape[0]
    df = df.replace({np.nan: None})

    print("importing videos from", path)
    for i in df.index:
        print("importing row: ", i, "/", total_rows, end="\r")
        query = """INSERT INTO videos (
            kind,
            etag,
            video_id,
            published_at,
            channel_id,
            title,
            description,
            tags,
            category_id,
            default_language,
            duration,
            licensed_content,
            content_rating,
            license,
            public_stats_viewable,
            made_for_kids,
            view_count,
            like_count,
            dislike_count,
            comment_count,
            topic_categories,
            created_at,
            updated_at
            ) VALUES (
            %s,%s,%s,%s,%s,%s,%s,%s,
            %s,%s,%s,%s,%s,%s,%s,%s,
            %s,%s,%s,%s,%s,%s,%s);
        """

        data = (
            df.loc[i, 'kind'],
            df.loc[i, 'etag'],
            df.loc[i, 'video_id'],
            df.loc[i, 'published_at'],
            df.loc[i, 'channel_id'],
            df.loc[i, 'title'],
            df.loc[i, 'description'],
            df.loc[i, 'tags'],
            df.loc[i, 'category_id'],
            df.loc[i, 'default_language'],
            df.loc[i, 'duration'],
            bool(df.loc[i, 'licensed_content']),
            df.loc[i, 'content_rating'],
            df.loc[i, 'license'],
            bool(df.loc[i, 'public_stats_viewable']),
            bool(df.loc[i, 'made_for_kids']),
            int(df.loc[i, 'view_count']) if df.loc[i, 'view_count'] else None,
            int(df.loc[i, 'like_count']) if df.loc[i, 'like_count'] else None,
            int(df.loc[i, 'dislike_count']) if df.loc[i, 'dislike_count'] else None,
            int(df.loc[i, 'comment_count']) if df.loc[i, 'comment_count'] else None,
            df.loc[i, 'topic_categories'],
            df.loc[i, 'created_at'],
            df.loc[i, 'updated_at']
        )

        helpers.insert_row(db_conn, query, data)
    print()
