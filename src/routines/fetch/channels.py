import numpy as np


def validate_channel_details(app_conf, db_conn, youtube, hp_acom, hp_api, hp_db):
    print("validating channels")
    cost = hp_acom.calc_quota_cost(app_conf, "validate_channels")
    amount_of_batches = int(
        np.ceil(app_conf.channel_limit / app_conf.channel_batch_size)
    )

    print("channel limit: ", app_conf.channel_limit)
    print("batch size: ", app_conf.channel_batch_size)
    print("cost: ", cost)
    print("days: ", cost / 10000)
    print("possible runs per day: ", int(10000 / cost))

    for batch_count in range(amount_of_batches):
        db_cur = db_conn.cursor()
        db_cur.execute("SELECT channel_id FROM channels WHERE validated_at IS NULL;")
        query_response = db_cur.fetchmany(app_conf.channel_batch_size)
        db_cur.close()

        channel_ids = [id for (id,) in query_response]
        if len(channel_ids) == 0:
            exit("all channels in database have been validated!")
        print("fetching batch no: ", batch_count+1, "/", amount_of_batches,  end="\r")

        api_response = hp_api.fetchChannelDetails(youtube, channel_ids)
        valid_channel_objs = hp_api.channel_objs_from_response(api_response)
        all_channel_objs = hp_api.append_invalid_channels(
            channel_ids,
            valid_channel_objs
        )

        for channel in all_channel_objs:
            hp_db.update_channel(db_conn, channel)

        db_conn.commit()
    print()


def update_channel_details(app_conf, db_conn, youtube, hp_acom, hp_api, hp_db):
    cost = hp_acom.calc_quota_cost(app_conf, "update_channels")
    amount_of_batches = int(
        np.ceil(app_conf.channel_limit / app_conf.channel_batch_size)
    )

    print("updating channel data older than ", app_conf.update_interval, " days")
    print("channel limit: ", app_conf.channel_limit)
    print("batch size: ", app_conf.channel_batch_size)
    print("cost: ", cost)
    print("days: ", cost / 10000)
    print("possible runs per day: ", int(10000 / cost))

    for batch_count in range(amount_of_batches):
        db_cur = db_conn.cursor()
        db_cur.execute("SELECT channel_id FROM channels WHERE validated_at < NOW() - INTERVAL '%s days' ORDER BY validated_at ASC LIMIT %s;", (app_conf.update_interval ,app_conf.channel_limit,))
        query_response = db_cur.fetchmany(app_conf.channel_batch_size)
        db_cur.close()

        channel_ids = [id for (id,) in query_response]

        if len(channel_ids) == 0:
            message = "all channels with data older than " + str(app_conf.update_interval) + " days have been updated!"
            exit(message)
        print("fetching batch no: ", batch_count+1, "/", amount_of_batches,  end="\r")

        api_response = hp_api.fetchChannelDetails(youtube, channel_ids)
        valid_channel_objs = hp_api.channel_objs_from_response(api_response)
        all_channel_objs = hp_api.append_invalid_channels(
            channel_ids,
            valid_channel_objs
        )

        for channel in all_channel_objs:
            hp_db.update_channel(db_conn, channel)

        db_conn.commit()
    print()
